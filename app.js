let globalurl="https://api.cloud.altbalaji.com";

let getEl=(selector,base=document)=>base.querySelector(selector);

let fetchData=(api,baseurl=globalurl)=>fetch(baseurl+api).then(res=>res.json()).catch((err)=>{
    if(api==="/sections/31?domain=IN&limit=50")getEl("#root").innerHTML=`<h1 class="error">OOPS! Something went wrong.</h1>`;
    return Promise.reject();
});


let style=document.createElement("style");
document.head.appendChild(style);
let stylesheet=style.sheet;

let currenttitle=null;
let showTitleDetails=(title)=>{
    currenttitle=title;
    if(stylesheet.cssRules.length===0)stylesheet.insertRule(".titlebox{display:none}");
    title.classList.add("showingtitle");
}

let getBack=()=>{
    stylesheet.deleteRule(0);
    currenttitle.classList.remove("showingtitle");
}



fetchData("/sections/31?domain=IN&limit=50").then(response=>{

      let rooter=getEl("#root")
    //for all lists;

      let Lists=response.lists;

      Lists.forEach((list)=>{
        let title=list.titles.default;

        if(list.visible==="true"){
            let listelem=document.createElement("div");
            listelem.classList.add(title.replace(/\s/g,"-"));
            listelem.classList.add("titlebox");
            listelem.innerHTML=`<h3><span class="header">${title}</span><span class="counter"></span><span class="back">Back</span></h3><div class="childlist"></div>`;
            let counter=getEl(".counter",listelem);
            counter.addEventListener("click",showTitleDetails.bind(null,listelem));
            getEl(".back",listelem).addEventListener("click",getBack);
            let childlist=getEl(".childlist",listelem);


            rooter.appendChild(listelem);

            if(list.external_id)fetchData(list.external_id).then((res)=>{

                let content=res.content;

                counter.innerText=`All(${content.length}) (click to open)`;
                
                content.forEach(pict=>{
                    let pictelem=document.createElement("div");
                    pictelem.classList.add("picture");
                    pictelem.innerHTML=`<img class="picture-image" src="${pict.images.find(x=>(x.type==="cover"&&x.format==="thumbnail-hd")).url}" alt="image not available"/>
                                          <h5>${pict.title}</h5>`;
                    childlist.appendChild(pictelem);

                })
               
                

            }).catch((err)=>{
               if(listelem)listelem.remove();
            })
        }
        
      })
       
})
